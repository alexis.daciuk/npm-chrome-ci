FROM centos:latest

# Agrego los proxies dentro del container
# RUN echo 'proxy=http://SRV_SRC_SERVER_USR_D:ahyQTRegf8@proxyserver.sis.ad.bia.itau:80' >> /etc/yum.conf

# Agrego el usuario y grupo de Jenkins para que coincida con el del slave
# asi soluciono el tema de los permisos
RUN groupadd -r -g 1000 jenkins
RUN adduser -r -u 1000 -g jenkins jenkins

# Instalo Node
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -

# Instalo dependencias necesarias
# gcc-c++ make
# RUN yum -y -v update
RUN yum install -y nodejs

RUN yum install -y  bash\
  chkconfig\
  ca-certificates\
  glibc\
  libX11\
  libXcomposite\
  libXdamage\
  libXext\
  libXfixes\
  libXrandr\
  alsa-lib\
  atk\
  at-spi2-atk\
  at-spi2-core\
  cairo\
  cups-libs\
  dbus-libs\
  libdrm\
  liberation-fonts\
  expat\
  mesa-libgbm\
  libgc\
  gtk3\
  gdk-pixbuf2\
  glib2\
  nspr\
  nss\
  nss-util\
  pango\
  vulkan-loader\
  libxcb\
  libxkbcommon\
  wget

# Instalo Google Chrome
RUN curl https://intoli.com/install-google-chrome.sh | bash

RUN yum clean all

COPY init.sh /tmp/init.sh
RUN chmod u+x /tmp/init.sh

ENV CHROME_BIN=/opt/google/chrome/google-chrome

USER jenkins

ENTRYPOINT ["/bin/bash", "/tmp/init.sh"]


