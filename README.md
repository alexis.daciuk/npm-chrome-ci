# README

Estos son los archivos para buildear el container npm-chrome-ci que se usa para los tests unitarios de frontend

## Como buildear

```sh
docker build -t itau/npm-chrome-ci .
```

## Como deployar

Para que la gente del banco pueda deployar el container al slave de Jenkins, hay que subirlo a dockerhub publico y pasarselo a la persona que lo necesite (actualmente Diego Bellini)

Lo ideal es en un futuro tener un pipeline que corra una vez por semana que rebuildee el container automaticamente

```sh
docker push alexdaciukglobant/npm-chrome-ci:latest
```