#!/bin/bash

cd /it0463-mercury-frontend/clients/

# Ejecuto los tests unitarios client
npm run test-ci
salida_test_ci=$?

# Ejecuto los tests unitarios de backoffice
npm run test-ci-backoffice
salida_test_ci_backoffice=$?

if [ $salida_test_ci -ne 0 ] || [ $salida_test_ci_backoffice -ne 0 ]; then
  exit 1
else
  exit 0
fi
